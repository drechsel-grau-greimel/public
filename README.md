# Downloads for our joint projects

## Falling Behind: Has Rising Inequality Fueled the American Debt Boom?

[paper](https://gitlab.com/drechsel-grau-greimel/public/builds/artifacts/master/raw/falling-behind/paper/falling-behind-paper.pdf?job=compile_pdf_slides)

[slides](https://gitlab.com/drechsel-grau-greimel/public/builds/artifacts/master/raw/falling-behind/slides/falling-behind-slides.pdf?job=compile_pdf_slides)
